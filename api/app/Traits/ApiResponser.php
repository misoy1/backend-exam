<?php

namespace App\Traits;

use Carbon\Carbon;

trait ApiResponser {

	protected function token($token, $code = 200)
	{
		$tokenData = [
			'token' => $token->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString()
        ];

        return response()->json($tokenData, 200);
    }

    protected function success($data, $code = 200, $withoutWrap = false)
	{
        $key = is_string($data) ? 'status' : 'data';
        $newDataFormat = $withoutWrap ? $data : [$key => $data];

		return response()->json($newDataFormat, $code);
	}

	protected function error($message = null, $code)
	{
        return response()->json([
            'message' => $message
        ], $code);
	}
}
