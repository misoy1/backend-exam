<?php

namespace App\Http\Controllers\Api;

use App\Models\Post;
use App\Http\Resources\PostResource;
use App\Http\Requests\PostRequest;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Exception;

class PostController extends Controller
{
    use ApiResponser;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api')->only(['store', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Post $post)
    {
        $post = Auth::check() ? $request->user()->posts(): $post;
        return PostResource::collection($post->paginate(15));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request, Post $post)
    {
        $data = $request->validated();
        $data['slug'] = $this->getSlug($request->title);
        $data['image'] = $request->image ?? 'https://www.eduprizeschools.net/wp-content/uploads/2016/06/No_Image_Available.jpg';

        $post = $request->user()->posts();
        $result = $post->create($data);

        $this->getUniqueSlug($post, $request->title);

        if ($result->exists) {
            return $this->success(new PostResource($post->find($result->id)), 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $slug)
    {
        try {
            $post = Post::slug($slug)->firstOrFail();
            $post = Auth::check() ? $request->user()->posts()->slug($slug)->firstOrFail() : $post;
            return $this->success(new PostResource($post));
        } catch (Exception $exception) {
            return $this->error($exception->getMessage(), 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        try {
            $post = Post::slug($slug)->firstOrFail();
            $slug = $request->title ? $this->getSlug($request->title) : $slug;
            $request['slug'] = $slug;

            $result = $post->update($request->all());

            $this->getUniqueSlug($post, $request->title);

            return $this->success(new PostResource($post));
        } catch (Exception $exception) {
            return $this->error($exception->getMessage(), 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $slug)
    {
        try {
            $post = Post::slug($slug)->firstOrFail()->delete();
            return $this->success('record deleted successfully');
        } catch (Exception $exception) {
            return $this->error($exception->getMessage(), 404);
        }
    }

    /**
     * Get slug from title.
     *
     * @param  string  $title
     * @return string
     */
    private function getSlug($title) {
        return Str::slug($title, '-');
    }

    /**
     * Make user's post slug unique.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    private function getUniqueSlug($post, $title) {
        $posts = $post->title($title)->get();

        if ($posts->count() > 1) {
            foreach ($posts as $key => $post) {
                $post->update([
                    'slug' => $this->getSlug($post->title) . '-' . ($key + 1)
                ]);
            }
        }
    }
}
