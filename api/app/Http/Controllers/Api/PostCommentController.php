<?php

namespace App\Http\Controllers\Api;

use App\Models\Post;
use App\Models\Comment;
use App\Http\Resources\PostCommentResource;
use App\Http\Requests\PostCommentRequest;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class PostCommentController extends Controller
{
    use ApiResponser;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api')->only(['store', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $post = Post::slug($request->post)->firstOrFail();

        return $this->success(PostCommentResource::collection($post->comments()->get()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostCommentRequest $request, Comment $comment)
    {
        $post = Post::slug($request->post)->firstOrFail();

        $data = $request->validated();
        $data['commentable_type'] = 'App\Models\Post';
        $data['post_id'] = $post->id;
        $data['user_id'] = $request->user()->id;

        $result = $comment->create($data);

        if ($result->exists) {
            return $this->success(new PostCommentResource($result));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $post = Post::slug($request->post)->firstOrFail();

            $comment = $post->comments()->findOrFail($request->comment);
            $result = $comment->update($request->all());

            return $this->success(new PostCommentResource($comment));
        } catch (Exception $exception) {
            return $this->error($exception->getMessage(), 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $post = Post::slug($request->post)->firstOrFail();
            $comment = $post->comments()->findOrFail($request->comment);
            $result = $comment->delete($request->comment);

            return $this->success('record deleted successfully');
        } catch (Exception $exception) {
            return $this->error($exception->getMessage(), 400);
        }
    }
}
