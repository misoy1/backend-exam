<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
            'updated_at' => $this->formatDate($this->updated_at),
            'created_at' =>$this->formatDate($this->created_at),
            'id' => $this->id
        ];
    }

    /**
     * Format date
     *
     * @param  datetime  $date
     * @return Carbon\Carbon
     */
    private function formatDate($date)  {
        return Carbon::parse($date)->toDateTimeString();
    }
}
