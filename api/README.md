# Blog API

This is a Blog API developed on Laravel 8.22.0. 
Users can use this API to: 
* Login
* Register 
* Retrieve, create, update and delete posts and comments

Read API documentation here (https://documenter.getpostman.com/view/78990/RznLHGgv)

# Getting started

## Installation

Clone the repository

    git clone [repository link]

Switch to the repo folder

    cd blog

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Run this command to create the encryption keys needed to generate secure access tokens

    php artisan passport:install
    
Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

**Command list**

    git clone [repository link]
    cd blog
    composer install
    cp .env.example .env
    php artisan key:generate
    
**Make sure you set the correct database connection information before running the passport isntall and migrations**

    php artisan migrate
    php artisan passport:install
    php artisan serve

## Database migration and seeding

Run the database migration and seeder and you're done

    php artisan migrate
    php artisan db:seed

***Note*** : It's recommended to have a clean database before seeding. You can refresh your migrations at any point to clean the database by running the following command
